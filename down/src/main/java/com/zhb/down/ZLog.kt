package com.zhb.down

import android.util.Log

/**
 * Created by HongboZhao on 2021/1/20.
 */
const val TAG = "Down"

object ZLog {

    //是否打印日志
    var isDebug = false

    fun d(msg: String) {
        if (isDebug) {
            Log.d(TAG, msg)
        }
    }

    fun e(msg: String, throwable: Throwable) {
        if (isDebug) {
            Log.d(TAG, msg, throwable)
        }
    }
}